import numpy as np

class LinearModule(object):
    """
    Linear module. Applies a linear transformation to the input data.
    """
    def __init__(self, in_features, out_features):
        """
        Initializes the parameters of the module.

        Args:
          in_features: size of each input sample
          out_features: size of each output sample

        Also, initialize gradients with zeros.
        """

        self.params = {'weight': np.random.normal(0, 0.0001, \
                                                  (out_features, in_features)),
                       'bias': np.zeros((out_features, 1))}
        self.grads = {'weight': np.zeros((out_features, in_features)),
                      'bias': np.zeros((out_features, 1))}

    def forward(self, x):
        """
        Forward pass.

        Args:
          x: input to the module
        Returns:
          out: output of the module
        """

        self._x = x
        out = x @ self.params['weight'].T + self.params['bias'].T

        return out

    def backward(self, dout):
        """
        Backward pass.

        Args:
          dout: gradients of the previous module
        Returns:
          dx: gradients with respect to the input of the module
        """

        self.grads['weight'] = dout.T @ self._x
        self.grads['bias'] = dout.T @ np.ones((dout.shape[0], 1))
        dx = dout @ self.params['weight']

        return dx

class ReLUModule(object):
    """
    ReLU activation module.
    """
    def forward(self, x):
        """
        Forward pass.

        Args:
          x: input to the module
        Returns:
          out: output of the module
        """

        self._out = np.maximum(0, x)

        return self._out

    def backward(self, dout):
        """
        Backward pass.

        Args:
          dout: gradients of the previous modul
        Returns:
          dx: gradients with respect to the input of the module
        """

        dx = dout * (self._out > 0)

        return dx

class SoftMaxModule(object):
    """
    Softmax activation module.
    """
    def forward(self, x):
        """
        Forward pass.
        Args:
          x: input to the module
        Returns:
          out: output of the module
        """

        sz = x.shape[0]
        max_x = np.reshape(x.max(axis=1), (sz, 1))

        num = np.exp(x - max_x)
        self._out = num / np.reshape(np.sum(num, axis=1), (sz, 1))

        return self._out

    def backward(self, dout):
        """
        Backward pass.

        Args:
          dout: gradients of the previous module
        Returns:
          dx: gradients with respect to the input of the module
        """

        diagonal = [np.diag(x) for x in self._out]

        derivative = diagonal - np.einsum('ij, ik -> ijk', self._out, self._out)

        dx = np.einsum('ij, ijk -> ik', dout, derivative)

        return dx

class CrossEntropyModule(object):
    """
    Cross entropy loss module.
    """
    def forward(self, x, y):
        """
        Forward pass.

        Args:
          x: input to the module
          y: labels of the input
        Returns:
          out: cross entropy loss
        """

        out = - np.sum(y * np.log(x + 1e-5)) / len(y)

        return out

    def backward(self, x, y):
        """
        Backward pass.

        Args:
          x: input to the module
          y: labels of the input
        Returns:
          dx: gradient of the loss with the respect to the input x.
        """

        dx = - y / (len(y) * (x + 1e-5))

        return dx
